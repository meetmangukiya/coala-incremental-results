FROM coala/base:latest
MAINTAINER Hemang S Kumar <hemangsk@gmail.com>

RUN mkdir -p /usr/src/app
COPY . /usr/src/app

RUN rm -rf coala-quickstart

RUN git clone --depth 1 https://github.com/coala/coala-quickstart.git \
&& time pip3 install --no-cache-dir -e coala-quickstart

WORKDIR /usr/src/app
CMD ["python3", "run.py"]
