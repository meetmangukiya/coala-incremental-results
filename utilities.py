from collections import defaultdict
import os
import tempfile

from coala_utils.Extensions import exts
from coala_utils.FileUtils import create_tempfile
from coalib.parsing.ConfParser import ConfParser

TEMP_DIR = os.getcwd() + '/tmp'


def coafile_to_json(directory_name):
    conf_writer = ConfParser()
    result = conf_writer.parse(directory_name + '/.coafile')
    response_json = defaultdict(lambda: {})

    for section in result.values():
        response_json["response"][section.name] = {}
        for setting in section:
            response_json["response"][section.name][section[setting].key] = section[setting].value

    return response_json


def sections_to_command(sections, cmd=[]):
    for section, content in sections.items():

        section_files = section + '.' + "files"
        cmd.append(section_files + '=' + content["files"])

        section_bears = section + '.' + "bears"
        cmd.append(section_bears + '=' + ','.join(content["bears"].keys()))

        for bear in content["bears"]:
            if len(content["bears"][bear]) > 0:
                for setting, value in content["bears"][bear].items():
                    section_setting = section + '.' + setting
                    cmd.append(section_setting + '=' + value)
    return cmd


def create_temp_dir_and_file(extension, dir=None):
    directory_name = tempfile.mkdtemp(dir=TEMP_DIR)
    filename = create_tempfile(
        prefix="tmp", suffix=extension, dir=directory_name)
    return directory_name, filename


def write_to_file(filename, content):
    with open(filename, 'w') as pointer:
        pointer.write(content)
        pointer.close()


def read_coafile(filename):
    with open(filename, 'r') as pointer:
        coafile = pointer.read()
        pointer.close()
        return coafile


def check_for_valid_extension(extension):
    for ext in exts:
        if list(exts[ext])[0].lower() == extension.lower():
            return True, ext

    return False, None


def get_language_extension(language):
    extensions = []
    for ext in exts:
        for lang in exts[ext]:
            if language == lang.lower():
                extensions.append(ext)

    extensions.sort()
    return extensions[0]
